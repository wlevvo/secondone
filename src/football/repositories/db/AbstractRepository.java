package football.repositories.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import football.DBConnection;
import football.entities.AbstractEntity;
import football.repositories.Repository;

public abstract class AbstractRepository<T extends AbstractEntity> implements Repository<T> {
	protected CallableStatement addCallableStatement;
	private CallableStatement deleteCallableStatement;
	protected PreparedStatement getPreparedStatement;
	private Class<?> repositoryClazz;

	protected abstract String getAddCallSql();

	protected abstract int getAddCallOutParamIndex();

	protected abstract String getDeleteCallSql();

	protected abstract String getGetPrepStmtSql();

	protected abstract void setAddCallableStatementParams(T entity) throws SQLException;

	protected abstract T getEntityFromResultSet(ResultSet resultSet) throws SQLException;

	public AbstractRepository(Class<?> repositoryClazz) {
		this.repositoryClazz = repositoryClazz;

		Connection connection = DBConnection.getConnection();
		try {
			addCallableStatement = connection.prepareCall(getAddCallSql());
			addCallableStatement.registerOutParameter(getAddCallOutParamIndex(), java.sql.Types.INTEGER);
			deleteCallableStatement = connection.prepareCall(getDeleteCallSql());
			getPreparedStatement = connection.prepareStatement(getGetPrepStmtSql());
		} catch (SQLException e) {
			throw new RuntimeException("B��d inicjalizacji " + repositoryClazz.getName());
		}
	}

	@Override
	public void add(T entity) {
		try {
			setAddCallableStatementParams(entity);
			addCallableStatement.execute();
			entity.setId(addCallableStatement.getInt(getAddCallOutParamIndex()));
		} catch (SQLException e) {
			throw new RuntimeException("B��d dodania encji do bazy");
		}
	}

	@Override
	public void delete(int id) {
		try {
			deleteCallableStatement.setInt(1, id);
			deleteCallableStatement.execute();
		} catch (SQLException e) {
			throw new RuntimeException("B��d usuwania danych z bazy " + repositoryClazz.getName());
		}
	}

	@Override
	public T get(int id) {
		try {
			getPreparedStatement.setInt(1, id);
			ResultSet resultSet = getPreparedStatement.executeQuery();
			if (resultSet.next()) {
				return getEntityFromResultSet(resultSet);
			} else {
				throw new RuntimeException("Brak w bazie encji o podanym id");
			}
		} catch (SQLException e) {
			throw new RuntimeException("B��d pobierania encji z bazy");
		}
	}

}
