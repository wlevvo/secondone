package football.examples;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import football.entities.Player;

public class StreamExamples {
	public static void runExamples() {

		List<Player> players = new ArrayList<>();

		players.add(new Player(1, "Adam", "Adamowski", new Date(1990, 1, 1), 10, null));
		players.add(new Player(2, "Bartosz", "Bartoszowski", new Date(1980, 1, 1), 10, null));
		players.add(new Player(3, "Cyprian", "Cyprianowski", new Date(1992, 1, 1), 11, null));

		players.stream().map(p -> p.getFirstName()).forEach(fn -> System.out.println(fn));

		System.out.println("Po filtrze imion rozpoczynaj�cych si� od 'am' (filtrowanie imion):");
		players.stream().map(p -> p.getFirstName()).filter(fn -> fn.endsWith("am"))
				.forEach(fn -> System.out.println(fn));

		System.out.println("Po filtrze imion rozpoczynaj�cych si� od 'am' (filtrowanie pilkarzy):");
		players.stream().filter(p -> p.getFirstName().endsWith("am")).map(p -> p.getFirstName())
				.forEach(fn -> System.out.println(fn));

		List<String> firstNames = players.stream().map(p -> p.getFirstName()).collect(Collectors.toList());
		System.out.println("Zwr�cona lista imion:");
		System.out.println(firstNames);

		Optional<Player> optionalPlayer = players.stream().filter(p -> p.getLastName().equals("Adaamowski"))
				.findFirst();

		if (optionalPlayer.isPresent()) {
			Player player = optionalPlayer.get();
			System.out.println("Zwr�cony pi�karz:");
			System.out.println(player.getFirstName() + " " + player.getLastName());
		} else {
			System.out.println("Nie ma takiego pi�karza");
		}

		/*
		 * 3*. Wybra� najwcze�niej urodzonego pi�karza (.min(...))
		 */
		Optional<Player> optionalEarliestBornPlayer = players.stream()
				.min((p1, p2) -> p1.getDateOfBirth()
						.compareTo(p2.getDateOfBirth()));
		if(optionalEarliestBornPlayer.isPresent()) {
			System.out.println(optionalEarliestBornPlayer.get().getFirstName());
		}
		/*
		 * 4*. Dorzu�my do klasy Player pole id. 
		 * Zmapujmy utworzon� list� pi�karzy do mapy postaci: <Integer, Player>, 
		 * gdzie kluczem b�dzie id. (Collectors.toMap(...)).
		 */
		Map<Integer, Player> playerById = players.stream()
				.collect(Collectors.toMap(p -> p.getId(), p -> p));
		/*
		 * 5*. Zmapowa� list� pi�karzy do mapy <String, List<Player>>, 
		 * gdzie kluczem jest imi�, a warto�ci� lista pi�karzy o takim imieniu.
		 */
		Map<String, List<Player>> playersByFirstName = players.stream()
				.collect(Collectors.groupingBy(p -> p.getFirstName()));
		
	}
}
